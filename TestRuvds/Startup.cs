using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Shared.Domain;
using Shared.Infrastructure;
using TestRuvds.SignalR;
using VirtualServersModule.Domain;
using VirtualServersModule.Domain.DomainServices;
using VirtualServersModule.Infrastructure;

namespace TestRuvds
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader());
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "TestRuvds API", Version = "v1" });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
            services.AddControllersWithViews()
                .AddControllersAsServices();
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });
            var connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContextPool<ServersDbContext>(b => b.UseSqlServer(connection, s => s.MigrationsAssembly(typeof(ServersDbContext).Assembly.GetName().Name))
                .EnableDetailedErrors());
            services.AddTransient<IUowFactory, UowFactory>();
            services.AddTransient<ITotalUsageCalculator, TotalUsageCalculator>();
            services.AddTransient<IVirtualServersRepository, VirtualServersRepository>();
            services.AddTransient<ICancellationTokeRepository, CancellationTokeRepository>();
            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceScopeFactory factory)
        {
            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "TestRuvds API V1");
                });
            }
            using (var scope = factory.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<ServersDbContext>();

                if (!env.IsDevelopment())
                {
                    context.Database.Migrate();
                }
                else
                {
                    context.Database.EnsureCreated();
                }
            }
            app.UseExceptionHandler("/api/v1/Error");
            app.UseStaticFiles();
            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }
            app.UseRouting();
            app.UseCors("CorsPolicy");
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
                endpoints.MapHub<VirtualServersHub>("/virtualServersHub");
            });
            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";
                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }
    }
}
