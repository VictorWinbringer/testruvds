using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace TestRuvds.SignalR
{
    public sealed class VirtualServersHub : Hub
    {
        public async Task ServersUpdated()
        {
            await Clients.All.SendAsync(nameof(ServersUpdated));
        }
    }
}