using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Shared.Domain;
using TestRuvds.Models;
using TestRuvds.SignalR;
using VirtualServersModule.Domain;
using VirtualServersModule.Domain.DomainServices;
using VirtualServersModule.Domain.Entities;

namespace TestRuvds.Controllers
{
    [ApiController]
    [Route("api/v1/virtual-servers")]
    public class VirtualServersController : ControllerBase
    {
        private readonly IVirtualServersRepository _repository;
        private readonly ITotalUsageCalculator _calculator;
        private readonly IHubContext<VirtualServersHub> _hub;
        private readonly IUowFactory _factory;

        public VirtualServersController(IVirtualServersRepository repository, ITotalUsageCalculator calculator, IHubContext<VirtualServersHub> hub, IUowFactory factory)
        {
            _repository = repository;
            _calculator = calculator;
            _hub = hub;
            _factory = factory;
        }

        /// <summary>
        /// Получает с сервер страницу с виртуальными серверами
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(VirtualServerPageModel))]
        [ProducesResponseType(400, Type = typeof(ValidationProblemDetails))]
        [ProducesResponseType((int)HttpStatusCode.UnprocessableEntity, Type = typeof(ProblemDetails))]
        public async Task<VirtualServerPageModel> Get()
        {
            var servers = await _repository.Get();
            var lifeTimes = servers.Select(x => x.Item1.LifeTime);
            var totalTime = _calculator.Calculate(lifeTimes);
            return new VirtualServerPageModel(new TotalUsageModel(totalTime), servers.Select(x => new ServerModel(x)));
        }

        /// <summary>
        /// Создает новый виртуальный сервер и возврашает id созданного сервера
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(400, Type = typeof(ValidationProblemDetails))]
        [ProducesResponseType((int)HttpStatusCode.UnprocessableEntity, Type = typeof(ProblemDetails))]
        public async Task<int> Post()
        {
            var id = await _repository.Create();
            await _hub.Clients.All.SendAsync(nameof(VirtualServersHub.ServersUpdated), Guid.NewGuid());
            return id.Value;
        }

        /// <summary>
        /// Устанавливает дату удаления виртуального сервера и возрвращает количество измененных серверов
        /// </summary>
        /// <param name="ids"> идентификаторы серверов</param>
        /// <returns></returns>
        [HttpPut("remove")]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(400, Type = typeof(ValidationProblemDetails))]
        [ProducesResponseType((int)HttpStatusCode.UnprocessableEntity, Type = typeof(ProblemDetails))]
        public async Task<int> Remove(RemoveServerModel[] ids)
        {
            var servers = ids.Select(i => (new VirtualServerId(i.Id), new RowVersion(i.Version)));
            var count = await _repository.Delete(servers);
            await _hub.Clients.All.SendAsync(nameof(VirtualServersHub.ServersUpdated), Guid.NewGuid());
            return count;
        }

    }
}

