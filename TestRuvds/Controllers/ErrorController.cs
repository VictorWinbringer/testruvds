using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using TestRuvds.Models;

namespace TestRuvds.Controllers
{
    [ApiController]
    [Route("api/v1/Error")]
    public class ErrorController : ControllerBase
    {
        private const int MAX_DEPTH = 200;

        public ErrorController()
        {
        }

        [HttpGet, HttpPost, HttpDelete, HttpHead, HttpOptions, HttpPatch, HttpPut]
        [ProducesResponseType((int)HttpStatusCode.UnprocessableEntity, Type = typeof(ErrorModel))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(string))]
        public ActionResult Get() => Handle();



        private ActionResult Handle()
        {
            var exceptionHandlerPathFeature =
                HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            if (exceptionHandlerPathFeature?.Error is ApplicationException ex)
                return new ObjectResult(
                        new ErrorModel()
                        {
                            Message = ex.Message,
                            Data = ex.Data
                        })
                { StatusCode = (int)HttpStatusCode.UnprocessableEntity };
            return new ObjectResult(Convert(exceptionHandlerPathFeature?.Error))
            {
                StatusCode = (int)HttpStatusCode.InternalServerError
            };
        }


        private string Convert(Exception ex)
        {
            var details = new List<string>();
            var exceptions = new List<Exception>();
            Add(ex, details, exceptions);
            var sb = new StringBuilder();
            var i = 0;
            foreach (var s in details)
            {
                i++;
                sb.Append($"{i}) ");
                sb.Append(s);
                sb.Append(Environment.NewLine);
            }
            return sb.ToString();
        }

        private void Add(Exception ex, List<string> details, List<Exception> exceptions)
        {
            if (ex == null)
                return;
            if (details.Count > MAX_DEPTH)
                return;
            if (exceptions.Contains(ex))
                return;
            details.Add(ex.Message);
            exceptions.Add(ex);
            if (ex is AggregateException aggregate)
            {
                foreach (var e in aggregate.InnerExceptions)
                {
                    Add(e, details, exceptions);
                }
            }
            Add(ex.InnerException, details, exceptions);
        }
    }
}
