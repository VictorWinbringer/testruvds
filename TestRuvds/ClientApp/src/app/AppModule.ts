import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from "./AppComponent";
import { VirtualServersComponent } from "./virtual-servers/VirtualServersComponent";
import { HubService } from "./virtual-servers/HubService";

@NgModule({
  declarations: [
    AppComponent,
    VirtualServersComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: "virtual-servers", component: VirtualServersComponent }
    ])
  ],
  providers: [
    HubService
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
