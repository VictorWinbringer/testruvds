interface Problem {
  errors: Map<string, string[]>;
  type: string;
  title: string;
  status: number;
  detail: string;
  instance: string;
  extensions: Map<string, any>;
}
