interface VirtualServer {
  id: number;
  created: Date;
  removed: Date;
  selected: boolean;
  version: string;
}