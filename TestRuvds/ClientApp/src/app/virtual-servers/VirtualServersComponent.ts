import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HubService } from "./HubService";

const CONTROLLER: string = "api/v1/virtual-servers";

@Component({
  selector: 'app-virtual-servers',
  templateUrl: './virtual-servers.component.html'
})
export class VirtualServersComponent {

  private hubService: HubService;
  private http: HttpClient;
  private baseUrl: string;

  public serversPage: VirtualServersPage;
  public now: Date = new Date();
  public problems: Problem[];
  public lastCreatedId: number;
  public lastRemvedCount: number;
  public total: Date;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, hubService: HubService) {
    this.hubService = hubService;
    this.http = http;
    this.baseUrl = baseUrl + CONTROLLER;
    this.Load(false);
    this.subscribeToEvents();
    setInterval(() => {
      this.now = new Date();
      if (!this.serversPage || !this.serversPage.totalUsage)
        return;
      let total = this.serversPage.totalUsage;
      if (total.stopped)
        return;
      total.seconds++;
      if (total.seconds > 59) {
        total.seconds = 0;
        total.minutes++;
        if (total.minutes > 59) {
          total.minutes = 0;
          total.hours++;
          if (total.hours > 23) {
            total.hours = 0;
            total.days++;
          }
        }
      }
    }, 1000);

  }

  private subscribeToEvents(): void {
    this.hubService.messageReceived.subscribe((x) => {
      this.Load(true);
    });
  }

  private get anySelected(): boolean {
    return !!this.serversPage.servers.some(s => !!s.selected);
  }

  private Load(saveCheckBoxes: boolean) {
    this.http.get<VirtualServersPage>(this.baseUrl)
      .subscribe(result => {
        if (this.serversPage && this.serversPage.servers && saveCheckBoxes && result && result.servers) {
          result.servers.forEach(x => {
            this.serversPage.servers.forEach(y => {
              if (x.id == y.id) {
                x.selected = y.selected; //Можно было конечно обе колекции к словарю преобразовать и по ключу сделать но так как тут колекции очень маленькие это не имеет смысла.
              }
            });
          });
        }
        this.serversPage = result;
      }, this.handle);
  }

  private Add() {
    this.http.post<number>(this.baseUrl, "").subscribe(result => {
      this.lastCreatedId = result;
      this.Load(true);
    },
      this.handle);
  }

  private Remove() {
    var ids = this.serversPage.servers.filter(s => s.selected);
    this.http.put<number>(this.baseUrl + "/remove", ids).subscribe(result => {
      this.lastRemvedCount = result;
      this.Load(false);
    },
      this.handle);
  }

  private handle(error: any) {
    console.error(error);
    this.problems = <Problem[]>error.error;
  }
}
