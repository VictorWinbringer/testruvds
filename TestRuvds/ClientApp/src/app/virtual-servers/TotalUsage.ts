interface TotalUsage {
  days: number;
  hours: number;
  minutes: number;
  seconds: number;
  stopped: boolean;
}