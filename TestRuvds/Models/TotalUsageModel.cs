using VirtualServersModule.Domain.Entities;

namespace TestRuvds.Models
{
    public class TotalUsageModel
    {
        public int Days { get; }

        public int Hours { get; }

        public int Minutes { get; }

        public int Seconds { get; }

        public bool Stopped { get; }

        public TotalUsageModel(int days, int hours, int minutes, int seconds, bool stopped)
        {
            Days = days;
            Hours = hours;
            Minutes = minutes;
            Seconds = seconds;
            Stopped = stopped;
        }

        public TotalUsageModel(TotalUsage usage) 
            : this(usage.Value.Days, usage.Value.Hours, usage.Value.Minutes, usage.Value.Seconds, !usage.IsOpen)
        {

        }

        public TotalUsageModel()
        {
            //For framework
        }
    }
}
