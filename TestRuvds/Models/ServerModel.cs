using System;
using VirtualServersModule.Domain.Entities;

namespace TestRuvds.Models
{
    public class ServerModel
    {
        public int Id { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Removed { get; set; }

        public byte[] Version { get; set; }

        public ServerModel(int id, DateTime created, DateTime? removed, byte[] version)
        {
            Id = id;
            Created = created;
            Removed = removed;
            Version = version;
        }

        public ServerModel((VirtualServer server, RowVersion) server) 
            : this(server.Item1.Id.Value, server.Item1.LifeTime.Begin, server.Item1.LifeTime.End, server.Item2.Value)
        {

        }
    }
}
