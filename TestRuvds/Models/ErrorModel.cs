using System.Collections;

namespace TestRuvds.Models
{
    public class ErrorModel
    {
        public string Message { get; set; }
        public IDictionary Data { get; set; }
    }
}
