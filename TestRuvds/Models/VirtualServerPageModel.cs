using System.Collections.Generic;
using System.Linq;
using VirtualServersModule.Domain.Entities;

namespace TestRuvds.Models
{
    public class VirtualServerPageModel
    {
        public TotalUsageModel TotalUsage { get; }
        public ServerModel[] Servers { get; }

        public VirtualServerPageModel(VirtualServersPage page) : this(
            new TotalUsageModel(page.TotalUsed),
            page.Servers.Select(s => new ServerModel(s)))
        {
        }

        public VirtualServerPageModel(
            TotalUsageModel totalUsage,
            IEnumerable<ServerModel> servers)
        {
            TotalUsage = totalUsage;
            Servers = servers.ToArray();
        }

        public VirtualServerPageModel()
        {
            //для фреймворка
        }
    }
}
