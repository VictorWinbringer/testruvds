﻿namespace TestRuvds.Models
{
    public class RemoveServerModel
    {
        public int Id { get; set; }

        public byte[] Version { get; set; }
    }
}