using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Shared.Domain;
using Shared.Infrastructure;
using VirtualServersModule.Domain;
using VirtualServersModule.Domain.Entities;

namespace VirtualServersModule.Infrastructure
{
    public class VirtualServersRepository : IVirtualServersRepository
    {
        private readonly ServersDbContext _context;
        private readonly ICancellationTokeRepository _cancellationTokeRepository;

        public VirtualServersRepository(ServersDbContext context, ICancellationTokeRepository cancellationTokeRepository)
        {
            _context = context;
            _cancellationTokeRepository = cancellationTokeRepository;
        }

        public async Task<VirtualServerId> Create()
        {
            var server = new VirtualServerDto(0, DateTime.UtcNow, null, null);
            _context.VirtualServers.Add(server);
            await _context.SaveChangesAsync(_cancellationTokeRepository.Get());
            return new VirtualServerId(server.Id);
        }

        public async Task<int> Delete(IEnumerable<(VirtualServerId, RowVersion)> ids)
        {
            var list = ids.ToList();
            if (list.Count == 0)
                return 0;
            var intIds = list.Select(x => x.Item1.Value).ToList();
            var removed = DateTime.UtcNow;
            var servers = await _context.VirtualServers
                .Where(s => s.Removed == null)
                .Where(s => intIds.Any(x => x == s.Id))
                .ToListAsync();
            foreach (var server in servers)
            {
                server.Removed = removed;
                var originalValue = list
                    .First(x => x.Item1.Value == server.Id)
                    .Item2
                    .Value;
                _context
                    .Entry(server)
                    .Property(x => x.Version)
                    .OriginalValue = originalValue;
            }

            return await _context.SaveChangesAsync(_cancellationTokeRepository.Get());
        }

        public async Task<List<(VirtualServer, RowVersion)>> Get()
        {
            var servers = await _context
                .VirtualServers
                .AsNoTracking()
                .ToListAsync();
            return servers.Select(s => (new VirtualServer(new VirtualServerId(s.Id), new Period(s.Created, s.Removed)), new RowVersion(s.Version))).ToList();
        }
    }
}
