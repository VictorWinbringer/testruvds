using System.Threading;

namespace Shared.Domain
{
    public interface ICancellationTokeRepository
    {
        CancellationToken Get();
    }
}
