﻿using System;

namespace Shared.Domain
{
    public abstract class NotNullClassBase<T> : SingleValueBase<T> where T : class
    {
        protected NotNullClassBase(T value) : base(value)
        {
            Validate();
        }

        private void Validate()
        {
            if (Value == null)
                throw new ApplicationException("Значение равно null");
        }
    }
}