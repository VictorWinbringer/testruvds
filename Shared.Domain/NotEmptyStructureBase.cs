﻿using System;

namespace Shared.Domain
{
    public abstract class NotEmptyStructureBase<T> : SingleValueBase<T> where T : struct
    {
        protected NotEmptyStructureBase(T value) : base(value)
        {
            Validate();
        }

        private void Validate()
        {
            if (Value.Equals(default(T)))
                throw new ApplicationException("Значение по умолчанию");
        }
    }
}