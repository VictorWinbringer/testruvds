using System.Threading.Tasks;

namespace Shared.Domain
{
    public interface IUowFactory
    {
        Task<IUnitOfWork> CreateReadCommited();
        Task<IUnitOfWork> CreateRepeatableRead();
        Task<IUnitOfWork> CrateSerializable();
    }
}
