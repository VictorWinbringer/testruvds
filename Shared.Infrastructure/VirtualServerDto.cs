﻿using System;

namespace Shared.Infrastructure
{
    public class VirtualServerDto
    {
        public int Id { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Removed { get; set; }

        public byte[] Version { get; set; }

        public VirtualServerDto(int id, DateTime created, DateTime? removed, byte[] version)
        {
            Id = id;
            Created = created;
            Removed = removed;
            Version = version;
        }
    }
}
