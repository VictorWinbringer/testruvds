using System.Data;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Shared.Domain;

namespace Shared.Infrastructure
{
    public class UowFactory : IUowFactory
    {
        private readonly ServersDbContext _context;
        private readonly ICancellationTokeRepository _repository;

        public UowFactory(ServersDbContext context, ICancellationTokeRepository repository)
        {
            _context = context;
            _repository = repository;
        }

        public Task<IUnitOfWork> CreateReadCommited()
        {
            return Create(IsolationLevel.ReadCommitted);
        }

        public Task<IUnitOfWork> CreateRepeatableRead()
        {
            return Create(IsolationLevel.RepeatableRead);
        }

        public Task<IUnitOfWork> CrateSerializable()
        {
            return Create(IsolationLevel.Serializable);
        }

        private async Task<IUnitOfWork> Create(IsolationLevel level)
        {
            var tran = await _context.Database.BeginTransactionAsync(level, _repository.Get());
            return new UnitOfWork(tran, _repository);
        }
    }
}
