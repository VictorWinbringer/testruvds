using System;
using Microsoft.EntityFrameworkCore;

namespace Shared.Infrastructure
{
    public class ServersDbContext : DbContext
    {
        public ServersDbContext(DbContextOptions<ServersDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ServersDbContext).Assembly);
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<VirtualServerDto> VirtualServers { get; set; }
    }


}
