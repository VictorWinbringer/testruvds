﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;
using Shared.Domain;

namespace Shared.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbContextTransaction _transaction;
        private readonly ICancellationTokeRepository _cancellationTokeRepository;

        public UnitOfWork(IDbContextTransaction transaction, ICancellationTokeRepository cancellationTokeRepository)
        {
            _transaction = transaction;
            _cancellationTokeRepository = cancellationTokeRepository;
        }
        public void Dispose()
        {
            _transaction.Dispose();
        }

        public Task CommitAsync()
        {
            return _transaction.CommitAsync(_cancellationTokeRepository.Get());
        }
    }
}