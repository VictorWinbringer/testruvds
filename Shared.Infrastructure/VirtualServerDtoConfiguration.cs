﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.Infrastructure
{
    public class VirtualServerDtoConfiguration : IEntityTypeConfiguration<VirtualServerDto>
    {
        public void Configure(EntityTypeBuilder<VirtualServerDto> builder)
        {
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Version)
                .IsRowVersion();
        }
    }
}