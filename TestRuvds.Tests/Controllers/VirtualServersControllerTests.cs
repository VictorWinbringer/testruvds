using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Moq;
using Shared.Domain;
using Shared.Infrastructure;
using TestRuvds.Controllers;
using TestRuvds.Models;
using TestRuvds.SignalR;
using VirtualServersModule.Domain.DomainServices;
using VirtualServersModule.Infrastructure;
using Xunit;

namespace TestRuvds.Tests.Controllers
{
    /// <summary>
    /// Интеграционные тесты.
    /// </summary>
    public class VirtualServersControllerTests
    {
        [Fact]
        public async Task GetMethod_Should_ReturnExpectedValue()
        {
            using (var context = CreateContext())
            {
                var fixture = new Fixture();
                var (hub, proxy) = CreateHub();
                var start = DateTime.UtcNow;
                var end = start.AddDays(1).AddHours(1).AddMinutes(1).AddSeconds(1);
                for (int i = 0; i < 1000; i++)
                {
                    var dto = fixture.Create<VirtualServerDto>();
                    dto.Created = start;
                    dto.Removed = end;
                    context.VirtualServers.Add(dto);
                }
                await context.SaveChangesAsync();
                var l = CreateLifeTime();
                var controller = new VirtualServersController(new VirtualServersRepository(context, l.Object), new TotalUsageCalculator(), hub.Object, CreateFactory().Object);

                var response = await controller.Get();
                var servers = await context.VirtualServers.ToListAsync();

                proxy.Verify(x => x.SendCoreAsync(
                    It.IsAny<string>(),
                    It.IsAny<object[]>(),
                    It.IsAny<CancellationToken>()
                    ), Times.Never);
                servers.Should().NotBeNull();
                servers.Count.Should().Be(1000);
                response.Should().NotBeNull();
                response.Servers.Should().NotBeNull();
                response.Servers.Length.Should().Be(servers.Count);
                servers.ForEach(s => response
                    .Servers
                    .Should()
                    .Contain(x => x.Id == s.Id &&
                                  x.Created == s.Created &&
                                  x.Removed == s.Removed &&
                                  x.Version.SequenceEqual(s.Version)
                                  )
                );
                response.TotalUsage.Should().NotBeNull();
                response.TotalUsage.Stopped.Should().BeTrue();
                response.TotalUsage.Days.Should().Be(1);
                response.TotalUsage.Hours.Should().Be(1);
                response.TotalUsage.Minutes.Should().Be(1);
                response.TotalUsage.Seconds.Should().Be(1);
            }
        }

        [Fact]
        public async Task AddMethod_Should_AddExpectedValue()
        {
            using (var context = CreateContext())
            {
                var (hub, proxy) = CreateHub();
                var l = CreateLifeTime();
                var controller = new VirtualServersController(new VirtualServersRepository(context, l.Object), new TotalUsageCalculator(), hub.Object, CreateFactory().Object);

                var response = await controller.Post();
                var servers = await context.VirtualServers.ToListAsync();
                var server = servers.FirstOrDefault(s => s.Id == response);

                proxy.Verify(x => x.SendCoreAsync(
                    It.Is<string>(y => y == nameof(VirtualServersHub.ServersUpdated)),
                    It.IsAny<object[]>(),
                    It.IsAny<CancellationToken>()
                    ), Times.Once);
                servers.Should().NotBeNull();
                servers.Count.Should().Be(1);
                server.Should().NotBeNull();
                server.Id.Should().Be(response);
                server.Removed.Should().BeNull();
                server.Created.Should().BeCloseTo(DateTime.UtcNow, TimeSpan.FromMinutes(1));
            }
        }

        [Fact]
        public async Task AddMethod_Should_UpdateAsExpected()
        {
            using (var context = CreateContext())
            {
                var fixture = new Fixture();
                var (hub, proxy) = CreateHub();
                var vs = fixture.Create<VirtualServerDto>();
                vs.Removed = null;
                context.VirtualServers.Add(vs);
                await context.SaveChangesAsync();
                var l = CreateLifeTime();
                var controller = new VirtualServersController(new VirtualServersRepository(context, l.Object), new TotalUsageCalculator(), hub.Object, CreateFactory().Object);

                var response = await controller.Remove(new[]
                    {new RemoveServerModel {Id = vs.Id, Version = vs.Version}});
                var servers = await context.VirtualServers.ToListAsync();
                var server = servers.FirstOrDefault(s => s.Id == vs.Id);

                proxy.Verify(x => x.SendCoreAsync(
                    It.Is<string>(y => y == nameof(VirtualServersHub.ServersUpdated)),
                    It.IsAny<object[]>(),
                    It.IsAny<CancellationToken>()
                ), Times.Once);
                servers.Should().NotBeNull();
                servers.Count.Should().Be(1);
                server.Should().NotBeNull();
                response.Should().Be(1);
                server.Removed.Should().NotBeNull()
                    .And.BeCloseTo(DateTime.UtcNow, TimeSpan.FromMinutes(1));
                server.Id.Should().Be(vs.Id);
                server.Created.Should().Be(vs.Created);
            }
        }

        private Mock<IUowFactory> CreateFactory()
        {
            var uow = new Mock<IUnitOfWork>();
            var factory = new Mock<IUowFactory>();
            factory.Setup(x => x.CreateReadCommited())
                .ReturnsAsync(uow.Object);
            uow.Setup(x => x.CommitAsync()).Returns(Task.CompletedTask);
            return factory;
        }

        private Mock<ICancellationTokeRepository> CreateLifeTime()
        {
            var m = new Mock<ICancellationTokeRepository>();
            m.Setup(x => x.Get()).Returns(CancellationToken.None);
            return m;
        }

        private (Mock<IHubContext<VirtualServersHub>>, Mock<IClientProxy>) CreateHub()
        {
            var hub = new Mock<IHubContext<VirtualServersHub>>(MockBehavior.Strict);
            var clients = new Mock<IHubClients>();
            var proxy = new Mock<IClientProxy>();
            hub
                .Setup(x => x.Clients)
                .Returns(clients.Object);
            clients
                .Setup(x => x.All)
                .Returns(proxy.Object);
            proxy
                .Setup(x => x.SendCoreAsync(
                    It.IsAny<string>(),
                    It.IsAny<object[]>(),
                    It.IsAny<CancellationToken>()
                    ))
                .Returns(Task.CompletedTask);
            return (hub, proxy);
        }

        private ServersDbContext CreateContext()
        {
            var databaseName = Guid.NewGuid().ToString();
            var options = new DbContextOptionsBuilder<ServersDbContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
            return new ServersDbContext(options);
        }
    }
}
