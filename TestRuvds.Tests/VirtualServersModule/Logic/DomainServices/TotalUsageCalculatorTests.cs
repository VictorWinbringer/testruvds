using System;
using System.Collections.Generic;
using FluentAssertions;
using Shared.Domain;
using VirtualServersModule.Domain.DomainServices;
using VirtualServersModule.Domain.Entities;
using Xunit;

namespace TestRuvds.Tests.VirtualServersModule.Logic.DomainServices
{
    public class TotalUsageCalculatorTests
    {
        [Fact]
        public void Calculate_Should_AddExpectedValue()
        {
            var calculator = new TotalUsageCalculator();
            var expectedDuration = TimeSpan.FromSeconds(1);
            var precession = TimeSpan.FromSeconds(1);
            var periods = new[] { new Period(DateTime.UtcNow, null) };

            var tu = calculator.Calculate(periods);

            tu.IsOpen.Should().BeTrue();
            tu.Value.Should().BeCloseTo(expectedDuration, precession);
        }

        [Fact]
        public void Calculate_Should_BeClosedIfAllPeriodsClosed()
        {
            var calculator = new TotalUsageCalculator();
            var expectedDuration = TimeSpan.FromDays(2);
            var precession = TimeSpan.FromSeconds(1);
            var periods = new[]
            {
                new Period(DateTime.UtcNow, DateTime.UtcNow.AddMinutes(1)),
                new Period(DateTime.UtcNow, DateTime.UtcNow.AddDays(1)),
                new Period(DateTime.UtcNow.AddDays(5), DateTime.UtcNow.AddDays(6))
            };

            var tu = calculator.Calculate(periods);

            tu.IsOpen.Should().BeFalse();
            tu.Value.Should().BeCloseTo(expectedDuration, precession);
        }

        [Fact]
        public void Calculate_Should_ThrowArgumentNullException()
        {
            var calculator = new TotalUsageCalculator();

            Assert.Throws<ApplicationException>(() => calculator.Calculate(null));
        }

        [Fact]
        public void Calculate_Should_OnEmptyArgumentBeEmpty()
        {
            var calculator = new TotalUsageCalculator();

            var tu = calculator.Calculate(new List<Period>());

            tu.IsOpen.Should().BeFalse();
            tu.Value.Should().Be(TimeSpan.Zero);
        }

        [Fact]
        public void Calculate_Should_OnDefaultPeriodThrowArgumentOutOfRangeException()
        {
            var calculator = new TotalUsageCalculator();
            var periods = new Period[] { null };

            Assert.Throws<ApplicationException>(() => calculator.Calculate(periods));
        }
    }
}
