using System.Collections.Generic;
using System.Threading.Tasks;
using Shared.Domain;
using VirtualServersModule.Domain.Entities;

namespace VirtualServersModule.Domain
{
    public interface IVirtualServersRepository
    {
        Task<VirtualServerId> Create();
        Task<int> Delete(IEnumerable<(VirtualServerId, RowVersion)> ids);
        Task<List<(VirtualServer, RowVersion)>> Get();
    }
}
