﻿using System;

namespace VirtualServersModule.Domain.Entities
{

    public class VirtualServer
    {
        private readonly VirtualServerId _id;
        private Period _lifeTime;

        public VirtualServer(VirtualServerId id) : this(id, new Period(DateTime.UtcNow, null))
        {
        }

        public VirtualServer(VirtualServerId id, Period lifeTime)
        {
            _id = id;
            _lifeTime = lifeTime;
            Validate();
        }

        public VirtualServerId Id => _id;
        public Period LifeTime => _lifeTime;

        public void Remove() => _lifeTime = new Period(_lifeTime.Begin, DateTime.UtcNow);

        private void Validate()
        {
            ValidateId();
            ValidateLifetime();
        }

        private void ValidateId()
        {
            if (Id == default)
                throw new ArgumentOutOfRangeException(nameof(Id), Id, "Id is default!");
        }

        private void ValidateLifetime()
        {
            if (LifeTime == default)
                throw new ArgumentOutOfRangeException(nameof(LifeTime), LifeTime, "Lifetime is default!");
        }
    }
}
