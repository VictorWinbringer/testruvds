﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VirtualServersModule.Domain.Entities
{
    public class VirtualServersPage
    {
        private readonly TotalUsage _totalUsage;
        private readonly List<(VirtualServer, RowVersion)> _servers;

        public VirtualServersPage(IEnumerable<(VirtualServer, RowVersion)> servers, TotalUsage totalUsage)
        {
            _totalUsage = totalUsage;
            _servers = servers?.ToList() ?? throw new ArgumentNullException(nameof(servers));
        }

        public IReadOnlyCollection<(VirtualServer, RowVersion)> Servers => _servers.AsReadOnly();

        public TotalUsage TotalUsed => _totalUsage;
    }
}
