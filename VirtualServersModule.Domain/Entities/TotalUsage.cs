﻿using System;
using System.Collections.Generic;
using Shared.Domain;

namespace VirtualServersModule.Domain.Entities
{
    public class TotalUsage : ValueObjectBase
    {
        public TotalUsage(TimeSpan value, bool isOpen)
        {
            Value = value;
            IsOpen = isOpen;
        }

        public TimeSpan Value { get; }

        public bool IsOpen { get; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Value;
            yield return IsOpen;
        }
    }
}
