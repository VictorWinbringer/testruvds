﻿using System;
using Shared.Domain;

namespace VirtualServersModule.Domain.Entities
{
    public class VirtualServerId : NotEmptyStructureBase<int>, IEquatable<VirtualServerId>
    {
        public VirtualServerId(int value) : base(value)
        {
            if (value < 1)
                throw new ApplicationException("value < 1")
                {
                    Data = { ["value"] = value }
                };
        }

        public int Value { get; }

        public static bool operator ==(VirtualServerId lhs, VirtualServerId rhl) => ValueObjectBase.Equals(lhs, rhl);
        public static bool operator !=(VirtualServerId lhs, VirtualServerId rhl) => !(lhs == rhl);

        public bool Equals(VirtualServerId other)
        {
            return this == other;
        }
    }
}
