﻿using System;
using System.Collections.Generic;
using Shared.Domain;

namespace VirtualServersModule.Domain.Entities
{
    public class RowVersion : NotNullClassBase<byte[]>
    {
        public RowVersion(byte[] value) : base(value)
        {
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            foreach (var b in Value)
            {
                yield return b;
            }
        }
    }
}