using System;
using System.Collections.Generic;
using Shared.Domain;

namespace VirtualServersModule.Domain.Entities
{
    public class Period : ValueObjectBase, IEquatable<Period>
    {
        public Period(DateTime begin, DateTime? end)
        {
            Begin = begin;
            End = end;
            if (Begin == default)
                throw new ApplicationException("Begin is default!");
            if (End.HasValue && End <= Begin)
                throw new ApplicationException("End <= Begin")
                {
                    Data = { ["begin"] = begin, ["end"] = end }
                };
        }

        public DateTime Begin { get; }
        public DateTime? End { get; }

        public bool Closed => End.HasValue;

        public TimeSpan Duration => End.HasValue ? End.Value - Begin : DateTime.UtcNow - Begin;

        public Period Union(Period period) => new Period(MinBegin(period), MaxEnd(period));

        private DateTime MinBegin(Period period) => Begin < period.Begin ? Begin : period.Begin;

        private DateTime? MaxEnd(Period period)
        {
            if (End.HasValue && period.End.HasValue)
            {
                return End.Value > period.End.Value ? End : period.End;
            }
            else
            {
                return (DateTime?)null;
            }
        }

        //Если один из периодов начинается внутри другого то они пересекаются.
        public bool Intersects(Period period) => StartedIn(period) || period.StartedIn(this);

        public bool SubsetOf(Period period) => StartedIn(period) && EndedIn(period);


        public bool StartedIn(Period period) => Begin >= period.Begin &&
            (!period.Closed || Begin <= period.End);

        public bool EndedIn(Period period) => End.HasValue && (!period.Closed || End <= period.End);

        public static bool operator ==(Period lhs, Period rhl) =>
            ValueObjectBase.Equals(lhs, rhl);

        public static bool operator !=(Period lhs, Period rhl) => !(lhs == rhl);

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Begin;
            yield return End;
        }

        public override bool Equals(object obj)
        {
            if (obj is Period id)
                return Equals(id);
            return false;
        }

        public bool Equals(Period obj) => obj == this;

        public override int GetHashCode()
        {
            return HashCode.Combine(Begin, End);
        }
    }
}
