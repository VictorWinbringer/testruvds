using System.Collections.Generic;
using VirtualServersModule.Domain.Entities;

namespace VirtualServersModule.Domain.DomainServices
{
    public interface ITotalUsageCalculator
    {
        TotalUsage Calculate(IEnumerable<Period> periods);
    }
}
