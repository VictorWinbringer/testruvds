using System;
using System.Collections.Generic;
using System.Linq;
using VirtualServersModule.Domain.Entities;

namespace VirtualServersModule.Domain.DomainServices
{
    public sealed class TotalUsageCalculator : ITotalUsageCalculator
    {
        public TotalUsage Calculate(IEnumerable<Period> periods)
        {
            if (periods == null)
                throw new ApplicationException( "periods is null!");
            var p = periods.ToList();
            if (p.Any(x => x == null))
                throw new ApplicationException("periods contains null period");
            //Обьеденяем мелкие и пересекающихся периоды вместе. В результат у нас остаются самые больше не пересекающиеся периоды.
            p = Union(p);
            var duration = Duration(p);
            var isOpen = IsOpen(p);
            return new TotalUsage(duration, isOpen);
        }

        //Объеденяет множество пересекаюшихся и не пересекашихся периодов в множество не пересекаюшихся.
        // Например если у нас был период  с 1 до 3 , c  2 до 3, с 4 до 5 то останется только с 1 до 3, с 4 до 5
        private static List<Period> Union(IEnumerable<Period> periods) => periods
            .OrderBy(x => x.Begin) 
            .ThenBy(x => x.End)
            .Aggregate(new Stack<Period>(), (s, p) =>
            {
                //Так как мы отсоритровали так что первым будет период который начинает раньше всех
                //поэтому помещяем его в начало стека как базовый он будет поглошать все следующие за ним если он с ними пересекается.
                if (s.Count == 0)
                {
                    s.Push(p);
                    return s;
                }
                var begin = s.Pop();
                //Если периоды пересекаются то сливаем их вместе и возрващаем в стек в виде нового периода.
                if (begin.Intersects(p))
                {
                    s.Push(begin.Union(p));
                }
                else
                {
                    //Если период не пересакается с тем что был до этого в стеке значит
                    // периоду который сейчас находится в стеке в принцыпе не скем больше пересекатся 
                    //потому что у нас колекция отсортирована по началу и потом по концу периода то дальше пересекающихся с ним периодов быть не может в принцыпе.
                    s.Push(begin);
                    s.Push(p);
                }
                return s;
            })
            .Distinct()
            .ToList();

        private static TimeSpan Duration(IEnumerable<Period> periods) => periods.Aggregate(TimeSpan.Zero, (x, y) => y.Duration + x);

        private static bool IsOpen(IEnumerable<Period> periods) => periods.Any(x => !x.Closed);
    }
}
